
<!DOCTYPE html>
<html lang="fr">
     <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" type="" href='vues/style.css'>
          <title>TP_final</title>
         
	</head>
	
	<body>

     <h1>Liste des film </h1>
          <div class="topnav">
          <a  href="index.php?target=accueil">Accueil</a>
          <a href="index.php?target=connexion">S'inscrire/Se connecter</a>
           
          </div>
         
    

          <table >
          <thead>
          <?php foreach($listColumn as $key=>$row) {?>
                    <th ><?php echo $row; ?>
                          <a href="index.php?orderBy=<?php echo $row; ?>&orderDirection=ASC"><i class="arrow up"></i> </a>
                          <a href="index.php?orderBy=<?php echo $row; ?>&orderDirection=DESC"><i class="arrow down"></i> </a>
                    </th>
                   
                    <?php }?>
          </thead>
          <tbody>
               <!--- affichage de la liste des films -->
               <!--- affichage des infos d'un film -->
                <?php foreach($data as $key=>$row) {?>
                      <tr>
                      <td> <?php echo $row["id"]?> </td>
                    <td>  <?php echo $row["nom"]?></td>
                    <td>  <?php echo $row["annee"]?></td>  <!-- affichage de l'année -->
                    <td> <?php echo $row["score"]?> </td> <!-- score -->
                    <td> <?php echo $row["nbVotants"]?> </td> <!-- nombre de votants -->

                          
                      </tr>
                <?php }?>
          </tbody>
          </table>
                    <!-- bouton se deconnecter-->
          <form action="index.php?target=connexion" method="POST">
			<button type="submit" name="deconnexion">Se deconnecter </button>
		</form>
     </body>
</html>