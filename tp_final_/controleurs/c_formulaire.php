<?php
 include_once("vues/v_formulaire.php");
 include_once('lib/fonctions.php');
$bdd = connectDb(); //connexion à la BDD
//modifier film si et seulement si user connnected

if (isset($_POST['connexion']) || isset($_POST['inscription'])){
if (!isset($_POST['id']) || !isset($_POST['nom']) 
|| !isset($_POST['score']) || !isset($_POST['annee'])
|| !isset($_POST['nbVotants']))
{
     echo "Veuiller remplir tous les champs " ;
     
}
else if (empty( ($_POST['id'])) || empty( ($_POST['nom']))
|| empty( ($_POST['score'])) || empty( ($_POST['annee']))
|| empty( ($_POST['nbVotants']))) {
     
          echo "un champ  est vide  " ;
}
else if (!!is_int((int)$_POST["id"]) || !is_float((float)$_POST["score"])   || !is_numeric($_POST['nbVotants']) || !is_numeric($_POST['annee'])){
     echo "Veuiller remplir tous les champs " ;
}
else if (strlen($_POST['id']) > 3 || strlen($_POST['nom']) > 20 || strlen($_POST['score']) > 3 || strlen($_POST['annee']) > 4 || strlen($_POST['nbVotants']) > 4) {
     echo "Veuiller remplir tous les champs " ;
}
else if (strlen($_POST['id']) < 3 || strlen($_POST['nom']) < 20 || strlen($_POST['score']) < 3 || strlen($_POST['annee']) < 4 || strlen($_POST['nbVotants']) < 4) {
     echo "Veuiller remplir tous les champs " ;
}
else if (!is_numeric($_POST['annee'])) {
     echo "Veuiller remplir tous les champs " ;
}
else if ($_POST['annee'] > date('Y')) {
     echo "Veuiller remplir tous les champs " ;
}

else {//if (strlen($_POST['id']) == 3 && strlen($_POST['nom']) == 20 && strlen($_POST['score']) == 3 && strlen($_POST['annee']) == 4 && strlen($_POST['nbVotants']) == 4) {
     if (isset($_POST['ajoutFilm'])) {
          $id = $_POST['id'];
          $nom = $_POST['nom'];
          $score = $_POST['score'];
          $annee = $_POST['annee'];
          $nbVotants = $_POST['nbVotants'];
          $query = $bdd->prepare('Insert into film values (?,?,?,?,?)');
          $query->execute(array($id,$nom,$annee,$score,$nbVotant));
          echo "Film ajouté";
     
          ajouterFilm($bdd, $id, $nom, $score, $annee, $nbVotants);
     }
     else if (isset($_POST['modifierFilm'])) {
          $id = $_POST['id'];
          $nom = $_POST['nom'];
          $score = $_POST['score'];
          $annee = $_POST['annee'];
          $nbVotants = $_POST['nbVotants'];
          $req = $bdd->prepare('UPDATE film SET nom = :nom, score = :score, annee = :annee, nbVotants = :nbVotants WHERE id = :id');
          $req->execute(array(
              'id' => $id,
              'nom' => $nom,
              'score' => $score,
              'annee' => $annee,
              'nbVotants' => $nbVotants
          ));
          $req->closeCursor();
          echo "film modifié";
     }
        else if (isset($_POST['supprimerFilm'])) {
            $id = $_POST['id'];
          $nom = $_POST['nom'];
          $query = $bdd->prepare('Delete from film where id = :id');
          $query->execute(array(
               'id' => $id,));
            supprimerFilm($bdd, $id);
        }
        else if (isset($_POST['voterFilm'])){
            voteFilm($bdd, $_POST['id'], $_POST['score']);
        }
    }
}
else {
     echo "Veuiller vous connectez " ;
}


?>