<?php
 include_once("vues/v_connexion_inscription.php");
 include_once('lib/fonctions.php');
$bdd = connectDb(); //connexion à la BDD
//formulaire d'inscription avec login mail et mdp si on clique sur le bouton s'inscrire

if (isset($_POST['inscription'])) {
    $login = $_POST['login'];
    $mail = $_POST['mail'];
    $mdp = $_POST['mdp'];
    $mdp2 = $_POST['mdp2'];
                 //cryptage du mdp
   // $verif = verifInscription($bdd, $login, $mail);    
   
    //verifie si le login et le mail sont déjà utilisés
    if (verifInscription($bdd, $login, $mail)) {
        echo "login ou mail déjà utilisé";
    }
   else if ($mdp !== $mdp2) {
        echo "les mots de passe ne correspondent pas";
    }
    else {
       // $mdp = password_hash($mdp, PASSWORD_DEFAULT);                  //cryptage du mdp non reussi

       // $mdp2 = password_hash($mdp2, PASSWORD_DEFAULT); 
       
        ajouterUtilisateur($bdd, $login, $mail, $mdp);
        echo "inscription réussie";
    }
}


//formulaire de connexion avec login mail et mdp si on clique sur le bouton se connecter

else if (isset($_POST['connexion']) and array_key_exists('connexion', $_POST)) {   //si on clique sur le bouton se connecter
    $login = $_POST['login'];       //on récupère le login
    $mdp = $_POST['mdp_c'];              //on récupère le mdp
    $req = $bdd->prepare('SELECT * FROM utilisateur WHERE login = :login');        //on récupère les données de l'utilisateur
    $req->execute(array(                                                        //on récupère les données de l'utilisateur
        'login' => $login                               //on récupère les données de l'utilisateur
    ));
    $data = $req->fetch();             
    $req->closeCursor();
    //var_dump($_SESSION['login']);
    if ($mdp == $data['email']) {                        
        $_SESSION['login'] = $data['login'];
        $_SESSION['email'] = $data['pwd'];
        $_SESSION['mdp_c'] = $data['email'];
       // $_SESSION['id'] = $data['id'];
        header('Location: index.php?target=accueil2');
        $connecte = true;
    } else {
        echo 'Mauvais login ou mot de passe !';
    }
}
if (isset($_POST['deconnexion'])) {
    session_destroy();
    header('Location: index.php');
}
//var_dump($_POST);


?>