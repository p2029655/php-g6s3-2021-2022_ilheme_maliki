<?php
 include_once("vues/v_inscription.php");
 include_once('lib/fonctions.php');
$bdd = connectDb(); //connexion à la BDD
//formulaire d'inscription avec login mail et mdp si on clique sur le bouton s'inscrire

if (isset($_POST['inscription'])) {
    $login = $_POST['login'];
    $mail = $_POST['mail'];
    $mdp = $_POST['mdp'];
    $mdp2 = $_POST['mdp2'];
    $mdp = password_hash($mdp, PASSWORD_DEFAULT);                  //cryptage du mdp
    $mdp2 = password_hash($mdp2, PASSWORD_DEFAULT);               //cryptage du mdp
    $verif = verifInscription($bdd, $login, $mail);              //verifie si le login et le mail sont déjà utilisés
    if ($verif == 0) {                  

    if ($mdp == $mdp2) {
        $mdp = password_hash($mdp, PASSWORD_DEFAULT);
        $req = $bdd->prepare('INSERT INTO utilisateurs(login, mail, mdp, nom) VALUES(:login, :mail, :mdp'); //requête d'insertion

        $req->execute(array(
            'login' => $login,
            'mail' => $mail,
            'mdp' => $mdp,
            
        ));
        $req->closeCursor();
        header('Location: index.php?target=connexion');
        echo 'Votre compte a bien été créé !';
    } else {
        echo 'Les mots de passe ne correspondent pas !';
    }               
    } else {
        echo 'Ce login ou cette adresse mail sont déjà utilisés !';
    }
}

//formulaire de connexion avec login mail et mdp si on clique sur le bouton se connecter

if (isset($_POST['connexion'])) {   //si on clique sur le bouton se connecter
    $login = $_POST['login'];       //on récupère le login
    $mdp = $_POST['mdp'];              //on récupère le mdp
    $req = $bdd->prepare('SELECT * FROM utilisateurs WHERE login = :login');        //on récupère les données de l'utilisateur
    $req->execute(array(                                                        //on récupère les données de l'utilisateur
        'login' => $login                               //on récupère les données de l'utilisateur
    ));
    $data = $req->fetch();             
    $req->closeCursor();
    if (password_verify($mdp, $data['mdp'])) {                      
        $_SESSION['login'] = $data['login'];
        $_SESSION['mail'] = $data['mail'];
        $_SESSION['nom'] = $data['nom'];
        $_SESSION['id'] = $data['id'];
        header('Location: index.php?target=accueil');
    } else {
        echo 'Mauvais login ou mot de passe !';
    }
}


?>




