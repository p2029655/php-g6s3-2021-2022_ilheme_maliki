<!-- faire le lien entre toutes les pages et le css-->
<!DOCTYPE html>
<html lang="fr">
     <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="css/style.css">
          <title>TP_final</title>
         
	</head>
	
	<body>

     <h1>Liste des films </h1>
          <div class="topnav">
          <a  href=#home>Acceuil</a>
          <a href="index.php?target=connexion">S'inscrire/Se connecter</a>
         
          <a href="index.php?target=formulaire">Ajouter un film</a>
          
          
         
          </div>
         
         
          

          <table >
          <thead>
          <?php foreach($listColumn as $key=>$row) {?>
                    <th ><?php echo $row; ?>
                          <a href="index.php?orderBy=<?php echo $row; ?>&orderDirection=ASC"><i class="arrow up"></i> </a>
                          <a href="index.php?orderBy=<?php echo $row; ?>&orderDirection=DESC"><i class="arrow down"></i> </a>
                    </th>
                   
                    <?php }?>
          </thead>
          <tbody>
               <!--- affichage de la liste des films -->
               <!--- affichage de la liste des films  avec une poubelle à cote pour supprimer -->
               <?php foreach($listFilm as $key=>$row) {?>
                    <tr>
                    <td> <?php echo $row["id"]?> </td>
                    <td>  <?php echo $row["nom"]?></td>
                    <td>  <?php echo $row["annee"]?></td>  <!-- affichage de l'année -->
                    <td> <?php echo $row["score"]?> </td> <!-- score -->
                    <td> <?php echo $row["nbVotants"]?> </td> <!-- nombre de votants -->
                         <td><a href="index.php?target=delete&id_film=<?php echo $row['id_film']; ?>">Supprimer</a></td>     <!-- supprimer -->
                    </tr>
                    <?php }?>
          </tbody>
          </table>

     </body>
</html>