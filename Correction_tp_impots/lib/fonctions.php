<?php
	function calculImpot($nbEnfants,$marie,$revenuNet){
		
		$nombrePart = $nbEnfants + $marie +1;
		
		$Q = $revenuNet / $nombrePart;
		
		$pourcentApplique = 45;
		
		foreach($GLOBALS["calculImpotTranche"] as $montant=>$pourcent) {
			if ($Q <= $montant){
				$pourcentApplique = $pourcent;
				break;
			}
		
		}
		
		$deductionApplique = 20163.45;
		
		foreach($GLOBALS["calculImpotDeduction"] as $montant=>$deduction ){
			if ($Q<=$montant) {
				$deductionApplique = $deduction;
				break;
			}
		}
		
		$montantImpot = $revenuNet * $pourcentApplique / 100 - $deductionApplique;
		
		return $montantImpot;
	
	}
?>