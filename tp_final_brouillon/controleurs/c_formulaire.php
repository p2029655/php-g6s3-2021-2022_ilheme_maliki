<?php
 include_once("vues/v_formulaire.php");
 include_once('lib/fonctions.php');
$bdd = connectDb(); //connexion à la BDD

if (!isset($_POST['id']) || !isset($_POST['nom']) 
|| !isset($_POST['score']) || !isset($_POST['annee'])
|| !isset($_POST['nbVotants']))
{
     echo "Veuiller remplir tous les champs " ;
     
}
else if (empty( ($_POST['id'])) || empty( ($_POST['nom']))
|| empty( ($_POST['score'])) || empty( ($_POST['annee']))
|| empty( ($_POST['nbVotants']))) {
     
          echo "un champ  est vide  " ;
}
else if (!is_numeric($_POST['id']) || !is_numeric($_POST['score'])   || !is_numeric($_POST['nbVotants'])) {
     echo "Veuiller remplir tous les champs " ;
}
else if (strlen($_POST['id']) > 3 || strlen($_POST['nom']) > 20 || strlen($_POST['score']) > 3 || strlen($_POST['annee']) > 4 || strlen($_POST['nbVotants']) > 4) {
     echo "Veuiller remplir tous les champs " ;
}
else if (strlen($_POST['id']) < 3 || strlen($_POST['nom']) < 20 || strlen($_POST['score']) < 3 || strlen($_POST['annee']) < 4 || strlen($_POST['nbVotants']) < 4) {
     echo "Veuiller remplir tous les champs " ;
}
else if (!is_numeric($_POST['annee'])) {
     echo "Veuiller remplir tous les champs " ;
}
else if ($_POST['annee'] > date('Y')) {
     echo "Veuiller remplir tous les champs " ;
}
else if (strlen($_POST['id']) == 3 && strlen($_POST['nom']) == 20 && strlen($_POST['score']) == 3 && strlen($_POST['annee']) == 4 && strlen($_POST['nbVotants']) == 4) {
     if (isset($_POST['ajoutFilm'])) {
          $id = $_POST['id'];
          $nom = $_POST['nom'];
          $score = $_POST['score'];
          $annee = $_POST['annee'];
          $nbVotants = $_POST['nbVotants'];
          ajouterFilm($bdd, $id, $nom, $score, $annee, $nbVotants);
     }
     else if (isset($_POST['modifierFilm'])) {
          $id = $_POST['id'];
          $nom = $_POST['nom'];
          $score = $_POST['score'];
          $annee = $_POST['annee'];
          $nbVotants = $_POST['nbVotants'];
          modifierFilm($bdd, $id, $nom, $score, $annee, $nbVotants);
     }



?>